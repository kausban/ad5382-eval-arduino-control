"""
Functions to initiate serial communication with Arduino to control Amplifier/DAC output
Created: 2021-02-23
Author: Kaustubh Banerjee
"""

module AmpCom
using LibSerialPort

# modify as needed
port = "/dev/ttyACM0"
baudrate = 115200
devices = 2

export
    AmpConnect,
    AmpUpdate,
    AmpZero,
    AmpReset,
    AmpStatus,
    AmpWrite,
    AmpClear

function AmpConnect(port=port, baudrate=baudrate)
    # Open serial communication if possible.
    # Returns SerialPort type object.
    mcu=SerialPort(port)
    if(isopen(mcu))
        println("Serial device already Open.")
        return(mcu)
    else
        return(open(port, baudrate))
    end
end

function AmpReset(sp::SerialPort)
    # send soft reset
    write(sp, "R")
    return(true)
end

function AmpClear(sp::SerialPort)
    # send soft clear
    write(sp, "C")
    return(true)
end

function AmpZero(sp::SerialPort)
    # Set voltage array on Arduino to 0 on all channels
    write(sp, "Z")
    return(true)
end

function AmpUpdate(sp::SerialPort)
    # write voltage array to DAC and pulse LDAC
    write(sp, "WL")
    return(true)
end

function check_varr(varr::Array{Int,1})
    # Test if voltage array is valid
    # Valid if: 0 <= voltage < 2^14
    # Array length is <= 32 * devices
    barr = (varr .>= 0) .* (varr .< 2^14)
    return(barr' * barr == length(barr) && length(barr) <= devices*32)
end

function AmpWrite(sp::SerialPort, varr::Array{Int,1})
    # send voltage array to arduino
    # Only accepts Integer vector
    if(check_varr(varr))
        cmd_message = ""
        for cnt=1:length(varr)
            cmd_message = string(cmd_message, varr[cnt], "/")
        end
        write(sp, cmd_message)
        return(true)
    else
        println("Check voltage array. Check Failed.")
        return(false)
    end
end

function AmpStatus(sp::SerialPort)
    # return state of voltage array on Arduino
    write(sp, "?")
    mcu_message = String(read(sp))

    if occursin("\n", mcu_message)
        lines = split(mcu_message, "\n")
        while length(lines) > 0
            println(popfirst!(lines))
        end
    end
end

end # AmpCom
