/*
  Control of the AD5382 EVAL board using arduino duo r3
  Created: 2021-01-29
  Author: Kaustubh Banerjee
*/
/*
  SPI communication with EVAL-AD5382
  13 - SCLK
  11 - MOSI/SDI
  12 - MISO/SDO //read - unused
  10 - Chip Select
   
  04 - CS/SYNC 1
  05 - CS/SYNC 2
  06 - CS/SYNC 3
  07 - CS/SYNC 4
   
  A0 - LDAC 1
  A1 - LDAC 2
  A2 - LDAC 3
  A3 - LDAC 4

  03 - BUSY // unused
  08 - RESET
  09 - CLR

  A/B  R/W  0    A4   A3   A2   A1  A0
  REG1 REG0 DB13 DB12 DB11 DB10 DB9 DB8
  DB7  DB6  DB5  DB4  DB3  DB2  DB1 DB0
*/

#include <SPI.h>
#include <math.h>

// 14 bit DAC 0 - 2^14
#define VMIN 0
#define VMAX 16383

// devices and channels
#define DEVS 2
#define CHAN 32*(DEVS)

// use internal reference
const bool IREF = false;

// set GPIO pins
const int SYNC1 = 4;
const int SYNC2 = 5;
const int SYNC3 = 6;
const int SYNC4 = 7;

const int LDAC1 = 14; //A0
const int LDAC2 = 15; //A1
const int LDAC3 = 16; //A2
const int LDAC4 = 17; //A3

//const int BUSY = 3;
const int RESET = 8;
const int CLR = 9;
const int CS = 10;

uint16_t voltages[CHAN] = {0};

void reset() {
  // Bringing the RESET line low resets the contents of all internal registers
  // to their power-on reset state. Reset is a negative edge-sensitive input.
  // The default corresponds to m at full scale and to c at zero scale. The
  // contents of the DAC registers are cleared, setting VOUT0 to VOUT31 to 0 V.
  // This sequence takes 270 μs max.
  digitalWrite(RESET, LOW);
  delay(1);
  digitalWrite(RESET, HIGH);
}

void clear() {
  // Bringing the CLR line low clears the contents of the DAC registers to the
  // data contained in the user-configurable CLR register and sets VOUT0 to
  // VOUT31 accordingly. This func-tion can be used in system calibration to
  // load zero scale and full-scale to all channels. The execution time for a
  // CLR is 35 μs Puts device in CLEAR state (all outputs to GND) enable = 1:
  digitalWrite(CLR, LOW);
  delay(1);
  digitalWrite(CLR, HIGH);
}

void load_dac(int keep = 0) {
  // Updated DAC output by pulsing LDAC low.
  // If keep = 1, the LDAC stays low -- causes immediate updates when new values are written
  // If keep = 0 (default), LDAC is pulsed causing written values to update the outputs.
  // new values are ignored until load_dac is called again.
  digitalWrite(LDAC1, LOW);
  digitalWrite(LDAC2, LOW);
  digitalWrite(LDAC3, LOW);
  digitalWrite(LDAC4, LOW);
  if (keep == 0) {
    delay(1);
    digitalWrite(LDAC1, HIGH);
    digitalWrite(LDAC2, HIGH);
    digitalWrite(LDAC3, HIGH);
    digitalWrite(LDAC4, HIGH);
  }
}

//SETUP
void setup() {
  Serial.begin(115200);
  // set output and input state of GPIOs:
  pinMode(SYNC1, OUTPUT);
  pinMode(SYNC2, OUTPUT);
  pinMode(SYNC3, OUTPUT);
  pinMode(SYNC4, OUTPUT);

  pinMode(RESET, OUTPUT);
  pinMode(CLR, OUTPUT);
  pinMode(CS, OUTPUT); //this pin should be set always as OUTPUT otherwise the SPI interface could be put automatically into slave mode by hardware

  pinMode(LDAC1, OUTPUT);
  pinMode(LDAC2, OUTPUT);
  pinMode(LDAC3, OUTPUT);
  pinMode(LDAC4, OUTPUT);

  // pinMode(BUSY, INPUT);
  
  SPI.begin();

  for(int i=0;i<CHAN;i++)
    voltages[i] = VMIN;

  digitalWrite(SYNC1, HIGH);
  digitalWrite(SYNC2, HIGH);
  digitalWrite(SYNC3, HIGH);
  digitalWrite(SYNC4, HIGH);

  soft_reset();
  soft_clear();
  if(IREF)
    internal_reference();

  /* reset(); */
  /* clear(); */
  load_dac();
}

void spi_write(uint8_t write_list[3], int chip_sel = 0) {
  // initialize SPI: 20 MHz, MSBFIRST, mode1
  SPI.beginTransaction(SPISettings(20000000, MSBFIRST, SPI_MODE1));

  switch (chip_sel){
  case 1:
    digitalWrite(SYNC1, LOW);
    break;
  case 2:
    digitalWrite(SYNC2, LOW);
    break;
  case 3:
    digitalWrite(SYNC3, LOW);
    break;
  case 4:
    digitalWrite(SYNC4, LOW);
    break;
  default:
    digitalWrite(SYNC1, LOW);
    digitalWrite(SYNC2, LOW);
    digitalWrite(SYNC3, LOW);
    digitalWrite(SYNC4, LOW);
    break;
  }

  SPI.transfer(write_list,3); // pointer to char array of size. send 3 bytes
  digitalWrite(SYNC1, HIGH);
  digitalWrite(SYNC2, HIGH);
  digitalWrite(SYNC3, HIGH);
  digitalWrite(SYNC4, HIGH);
  SPI.endTransaction();
}

void internal_reference(){
  // REG1 = REG0 = 0, A4–A0 = 01100
  // CR13: [1] Power down status. (0) 100kOhm (1) High impedence
  // CR12: [1] Ref select (0) 1.25 V or (1) 2.5 V
  // CR11: [0] Current boost mode
  // CR10: [1] Enable internal reference
  // CR9:CR8 [0 0] Channel monitor and thermal monitor
  // CR7:CR6 [0 0] do not care
  // CR5:CR2 [0 0 0 0] Toggle function enable
  // CR1:CR0 [0 0] do not care
  uint8_t write_list[3];
  write_list[0] = B00001100;
  write_list[1] = B00110100;
  write_list[2] = B00000000;

  spi_write(write_list);
}

void soft_reset() {
  // REG1 = REG0 = 0, A4–A0 = 01111 DB13–DB0 = Don’t Care
  uint8_t write_list[3];
  write_list[0] = B00001111;
  write_list[1] = B00000000;
  write_list[2] = B00000000;

  spi_write(write_list);
}

void soft_clear() {
  // REG1 = REG0 = 0, A4–A0 = 00010 DB13–DB0 = Don’t Care
  uint8_t write_list[3];
  write_list[0] = B00000010;
  write_list[1] = B00000000;
  write_list[2] = B00000000;

  spi_write(write_list);
}

void write_value_int(uint8_t output, uint16_t value, int chip_sel) {
  // Write DAC value to specific output pin
  // output: Pin to output. 0-31
  // value: Value to write. 0-16383
  // immediate: 1 = Output to pin now
  //            0 = Wait for load_dac() (default)

  if (value > 16383)
    value=16383;

  uint8_t x = B11000000;

  uint8_t write_list[3];
  write_list[0] = output;
  write_list[1] = x + uint8_t(value >> 8);
  write_list[2] = value & 0xff;

  /* Serial.println("\nwrite_value\n"); */
  /* Serial.print(write_list[0],BIN); Serial.println(" "); */
  /* Serial.print(write_list[1],BIN); Serial.println(" "); */
  /* Serial.print(write_list[2],BIN); Serial.println(" "); */

  spi_write(write_list, chip_sel);
}

void write_voltages(){
  // write voltage array to DAC

  for(int i=0;i<32 && i<CHAN;i++)
    write_value_int(i,voltages[i], 1);

  for(int i=32;i<64 && i<CHAN;i++)
    write_value_int(i-32,voltages[i], 2);

  for(int i=64;i<3*96 && i<CHAN;i++)
    write_value_int(i-64,voltages[i], 3);

  for(int i=96;i<128 && i<CHAN;i++)
    write_value_int(i-96,voltages[i], 4);
}

void write_value_all(uint16_t value) {
  // Write DAC value to all
  // value: Value to write. 0-16383

  for(int i=0;i<CHAN;i++)
    voltages[i] = value;

  write_voltages();
}

uint16_t serialdata = 0;
uint8_t channel=0;
uint8_t cnt=0;

void loop() {
  // R: soft reset. Set voltage array to 0.
  // C: soft clear
  // ?: current voltage array
  // W: write array to DACs
  // L: LDAC
  // A: Set all channels to 15657
  // B: Set all channels to 7876
  // Z: Set all channels to 0
  // @: Set previous serial data to $channel
  // H: Set previous serial data to all channels
  // 5000H: Set all channel to 5000
  // V: Set previous serial data to $voltage
  // 25@7000V: Sets channel 25 to 7000
  char inbyte=0;
  inbyte = Serial.read();
  switch (inbyte){
  case '/':
    voltages[cnt] = serialdata;
    serialdata = 0;
    cnt++;
    if(cnt == CHAN)
      cnt = 0;
    break;
  case '@':
    channel = serialdata;
    serialdata = 0;
    break;
  case 'V':
    voltages[channel] = serialdata;
    serialdata = 0;
    break;
  case 'R':
    soft_reset();
    if (IREF)
      internal_reference();
    write_value_all(VMIN);
    Serial.println("Soft Reset"); Serial.println(" ");
    break;
  case 'C':
    soft_clear();
    Serial.println("Soft Clear"); Serial.println(" ");
    break;
  case 'W':
    write_voltages();
    cnt = 0;
    break;
  case 'L':
    load_dac();
    break;
  case 'A':
    write_value_all(15657);
    Serial.println("Write 15657"); Serial.println(" ");
    break;
  case 'B':
    write_value_all(7876);
    Serial.println("Write 7876"); Serial.println(" ");
    break;
  case 'Z':
    write_value_all(0);
    Serial.println("Write 0"); Serial.println(" ");
    break;
  case 'H':
    write_value_all(serialdata);
    Serial.print("Write "); Serial.print(serialdata); Serial.println(" ");
    serialdata = 0;
    break;
  case '?':
    Serial.println("SerialData: "); Serial.print(serialdata); Serial.println(" ");
    Serial.println("Channel"); Serial.print(channel); Serial.println(" ");
    Serial.println("Counter"); Serial.print(cnt); Serial.println(" ");
    readArray();
    break;
  default:
    if (inbyte > 0)
      serialdata = serialdata * 10 + inbyte - '0';
    break;
  }
}

void readArray(){
  // Serial print voltage array contents
  Serial.println("Chan \t|\t Array 1 - Array 2 - Array 3 - Array 4");
  for(int i=0;i<32;i++){
    Serial.print(i);
    Serial.print(" \t|\t ");
    Serial.print(voltages[i]);
    Serial.print("\t");
    if(CHAN > 32) Serial.print(voltages[i+32]);
    Serial.print("\t");
    if(CHAN > 64) Serial.print(voltages[i+32+32]);
    Serial.print("\t");
    if(CHAN > 96) Serial.print(voltages[i+32+32+32]);
    Serial.println(" ");
  }
  Serial.flush();
}
