# Control of the AD5382 EVAL board using Arduino Duo R3

Arduino program that translates USB serial commands to SPI commands to
control up to **four** AD5382 evaluation boards for up to 128
individually addressed voltage signal channels.

Please check out the comments in the code for details.

## Notable variables

| Name | Valid value | Use                                   |
|------|-------------|---------------------------------------|
| DEVS | 1, 2, 3, 4  | Number of connected devices           |
| IREF | true, false | Use internal voltage reference or not |
| VMIN | 0-16383     | Minimum voltage for the 14 bit DAC    |
| VMAX | 0-16383     | Maximum voltage for the 14 bit DAC    |

## Serial instruction set

Expects to receive serial commands over USB for operation and control.
Note that the serial baud rate is set to 115200 bps.

| Instruction    | Effect                                           |
|----------------|--------------------------------------------------|
| R              | soft reset; set voltage array to VMIN            |
| C              | soft clear                                       |
| '?'            | return current state of voltage array            |
| W              | write voltage array to DAC(s)                    |
| L              | pulse LDAC                                       |
| A              | set all channels to 15657                        |
| B              | set all channels to 7876                         |
| Z              | set all channels to 0                            |
| '@'            | set previous serial data to $channel             |
| H              | set previous serial data to all channels         |
| 5000H          | set all channel to 5000H                         |
| V              | set previous serial data to $voltage             |
| 25@7000V       | set channel 25 to 7000                           |
| '/'            | serial data separator                            |
| 1000/3000/7000 | sets channel 0 to 1000, 1 to 3000, and 2 to 7000 |
| WL             | write voltage array to DAC(s) and pulse LDAC     |

## Pin Map

| Pin | Signal Name | Comments                              |
|-----|-------------|---------------------------------------|
| 13  | SCLK        | Arduino SPI Clock                     |
| 11  | MOSI/SDI    | Arduino SPI Out                       |
| 12  | MISO/SDO    | Arduino SPI In; unused                |
| 10  | Chip Select | Arduino SS pin; Don't use             |
| 04  | CS/SYNC 1   | DAC select. Set LOW before SPI write. |
| 05  | CS/SYNC 2   | DAC select. Set LOW before SPI write. |
| 06  | CS/SYNC 3   | DAC select. Set LOW before SPI write. |
| 07  | CS/SYNC 4   | DAC select. Set LOW before SPI write. |
| A0  | LDAC 1      | Toggle LOW to update DAC1             |
| A1  | LDAC 2      | Toggle LOW to update DAC2             |
| A2  | LDAC 3      | Toggle LOW to update DAC3             |
| A3  | LDAC 4      | Toggle LOW to update DAC4             |
| 03  | BUSY        | Read; unused                          |
| 08  | RESET       | Toggle LOW to Reset                   |
| 09  | CLR         | Toggle LOW to Clear                   |

## References from Analog Devices:

-   [Evaluation board
    webpage](https://www.analog.com/en/design-center/evaluation-hardware-and-software/evaluation-boards-kits/eval-ad5382.html)
-   [AD5382 webpage](https://www.analog.com/en/products/ad5382.html)
